@author Matt Perl

This will be an attempt at a sidescrolling game, hopefully with some fun mechanics

Features to implement:
- Randomly generated hazards (currently statically spawned)
  - designate spawns when sprite reaches next screen
- Score based on number of screens passed + obstacles dodged
- Additional types of hazards
  - lava blocks
  - cracked floors 

Outside sources:
See contributors.txt
