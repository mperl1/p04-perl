//
//  AppDelegate.h
//  Basic_Sidescroller
//
//  Created by Matt Perl on 3/11/17.
//  Copyright © 2017 Matt Perl. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

