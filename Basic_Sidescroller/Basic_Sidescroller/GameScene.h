//
//  GameScene.h
//  Basic_Sidescroller
//
//  Created by Matt Perl on 3/11/17.
//  Copyright © 2017 Matt Perl. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>

@interface GameScene : SKScene <SKPhysicsContactDelegate>

//@property (nonatomic) SKAction *walkLeftAnimation;
@property (nonatomic) SKAction *walkAnimation;
@property (nonatomic) SKAction *jumpAnimation;
//@property (nonatomic) SKNode *character;

@end
