//
//  GameScene.m
//  Basic_Sidescroller
//
//  Created by Matt Perl on 3/11/17.
//  Copyright © 2017 Matt Perl. All rights reserved.
//

#import "GameScene.h"

/*
@interface GameScene () <SKPhysicsContactDelegate>{
    
    
}

@end
*/
@implementation GameScene {
    SKSpriteNode *spikeHaz;
    SKSpriteNode *bearHaz;
    SKSpriteNode *waterHaz;
    SKLabelNode *gameOverLabel;
    SKLabelNode *scoreLabel;
}

//score vars
int score;
double lastPos = 0;

//reset
bool resetGame = false;

//Create obstacles
int distFromFrames = 300;
int distBetweenObstacles = 185;

//collision bitmasks
static const uint32_t charCategory = 1 << 0;
static const uint32_t spikeCategory = 1 << 1;
static const uint32_t bearCategory = 1 << 2;
static const uint32_t waterCategory = 1 << 3;


- (void)didMoveToView:(SKView *)view {
    // Setup your scene here
    
    //[self setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"really-lame-background.png"]]];
    
    self.physicsWorld.contactDelegate = self;
    
    //Anchor scene to background
    SKNode *background = [self childNodeWithName:@"background1"];
    //SKPhysicsBody *border = [SKPhysicsBody bodyWithEdgeLoopFromRect:background.frame];
    SKPhysicsBody *border = [SKPhysicsBody bodyWithEdgeLoopFromRect:CGRectMake(background.frame.origin.x-775, background.frame.origin.y+60, (background.frame.size.width*4)+775, background.frame.size.height)];
    //SKPhysicsBody *border = [SKPhysicsBody bodyWithRectangleOfSize:CGSizeMake(background.frame.size.width, background.frame.size.height+130)];
    self.physicsBody = border;
    
    //Give slight slip to friction
    self.physicsBody.friction = .8f;
     
    NSMutableArray<SKTexture *> *walkTextures = [[NSMutableArray alloc] init];
    
    //loop through the number of walking right animations
    for(int i = 1; i < 5; ++i){
        [walkTextures addObject:[SKTexture textureWithImageNamed:[NSString stringWithFormat:@"Eggy-walk%d",i]]];
    }
    self.walkAnimation = [SKAction repeatActionForever:[SKAction animateWithTextures:walkTextures timePerFrame:0.1]];
    //self.walkRightAnimation = [SKAction animateWithTextures:walkRightTextures timePerFrame:0.1];
    
    NSMutableArray<SKTexture *> *jumpTextures = [[NSMutableArray alloc] init];
    //loop through the number of jumping animations
    for(int i = 1; i < 6; ++i){
        [jumpTextures addObject:[SKTexture textureWithImageNamed:[NSString stringWithFormat:@"Eggy-jump%d",i]]];
    }
    [jumpTextures addObject:[SKTexture textureWithImageNamed:[NSString stringWithFormat:@"Eggy-walk1"]]];
    self.jumpAnimation = [SKAction animateWithTextures:jumpTextures timePerFrame: 0.1];
    
    
    //Create the camera
    SKNode *character = [self childNodeWithName:@"eggy"];
    character.physicsBody.categoryBitMask = charCategory;
    character.physicsBody.collisionBitMask = spikeCategory | bearCategory | waterCategory;
    character.physicsBody.contactTestBitMask = spikeCategory | bearCategory | waterCategory;
    SKCameraNode *camera = (SKCameraNode *)[self childNodeWithName:@"MainCamera"];
    
    //Make sure camera stays near character horizontally & vertically
    id horizontalConstraint = [SKConstraint distance:[SKRange rangeWithUpperLimit:100] toNode:character];
    id verticalConstraint = [SKConstraint distance: [SKRange rangeWithUpperLimit: 50] toNode:character];
    
    //Make sure camera doesn't go too far left or down
    id leftConstraint = [SKConstraint positionX:[SKRange rangeWithLowerLimit:camera.position.x-775]];
    id bottomConstraint = [SKConstraint positionY:[SKRange rangeWithLowerLimit:camera.position.y]];
    
    //Set top and right constraints
    id rightConstraint = [SKConstraint positionX:[SKRange rangeWithUpperLimit:(background.frame.size.width*4)]];
    id topConstraint = [SKConstraint positionY:[SKRange rangeWithUpperLimit:(background.frame.size.height-camera.position.y)]];
    
    [self.camera setConstraints:@[horizontalConstraint, verticalConstraint, leftConstraint, bottomConstraint, rightConstraint, topConstraint]];
    
    //Create obstacles
    int lastHazX = background.frame.origin.x+400;
    int lastHazY = background.frame.origin.y+60;
    for(int i = 0; i < 3; ++i){
        
        int hazType = arc4random_uniform(4);
                
        if(hazType == 0){
            spikeHaz = [SKSpriteNode spriteNodeWithTexture:[SKTexture textureWithImageNamed:[NSString stringWithFormat:@"gross-spikes"]]];
            spikeHaz.position = CGPointMake(lastHazX, lastHazY+50);
            spikeHaz.physicsBody = [SKPhysicsBody bodyWithTexture:spikeHaz.texture size:CGSizeMake(spikeHaz.texture.size.width, spikeHaz.texture.size.height)];
            spikeHaz.physicsBody.dynamic = NO;
            spikeHaz.physicsBody.categoryBitMask = spikeCategory;
            spikeHaz.physicsBody.contactTestBitMask = charCategory;
            [self addChild:spikeHaz];
            lastHazX += spikeHaz.frame.size.width + 400;
        }
        else if(hazType == 1){
            bearHaz = [SKSpriteNode spriteNodeWithTexture:[SKTexture textureWithImageNamed:[NSString stringWithFormat:@"Icee"]]];
            bearHaz.position = CGPointMake(lastHazX, lastHazY);
            bearHaz.physicsBody = [SKPhysicsBody bodyWithTexture:bearHaz.texture size:CGSizeMake(bearHaz.texture.size.width, bearHaz.texture.size.height)];
            bearHaz.physicsBody.dynamic = NO;
            bearHaz.physicsBody.categoryBitMask = bearCategory;
            bearHaz.physicsBody.contactTestBitMask = charCategory;
            [self addChild:bearHaz];
            lastHazX += bearHaz.frame.size.width + 400;
        }
        else if(hazType == 2){
            waterHaz = [SKSpriteNode spriteNodeWithTexture:[SKTexture textureWithImageNamed:[NSString stringWithFormat:@"more-puddle"]]];
            waterHaz.position = CGPointMake(lastHazX, lastHazY-25);
            waterHaz.physicsBody = [SKPhysicsBody bodyWithTexture:waterHaz.texture size:CGSizeMake(waterHaz.texture.size.width, waterHaz.texture.size.height)];
            waterHaz.physicsBody.dynamic = NO;
            waterHaz.physicsBody.categoryBitMask = waterCategory;
            waterHaz.physicsBody.contactTestBitMask = charCategory;
            [self addChild:waterHaz];
            lastHazX += waterHaz.frame.size.width + 400;
        }
    }
    
    
    SKNode *bg2 = [self childNodeWithName:@"background2"];
    lastHazX = bg2.frame.origin.x+400;
    for(int i = 0; i < 3; ++i){
        
        int hazType = arc4random_uniform(4);
        
        if(hazType == 0){
            spikeHaz = [SKSpriteNode spriteNodeWithTexture:[SKTexture textureWithImageNamed:[NSString stringWithFormat:@"gross-spikes"]]];
            spikeHaz.position = CGPointMake(lastHazX, lastHazY+50);
            spikeHaz.physicsBody = [SKPhysicsBody bodyWithTexture:spikeHaz.texture size:CGSizeMake(spikeHaz.texture.size.width, spikeHaz.texture.size.height)];
            spikeHaz.physicsBody.dynamic = NO;
            spikeHaz.physicsBody.categoryBitMask = spikeCategory;
            spikeHaz.physicsBody.contactTestBitMask = charCategory;
            [self addChild:spikeHaz];
            lastHazX += spikeHaz.frame.size.width + 400;
        }
        else if(hazType == 1){
            bearHaz = [SKSpriteNode spriteNodeWithTexture:[SKTexture textureWithImageNamed:[NSString stringWithFormat:@"Icee"]]];
            bearHaz.position = CGPointMake(lastHazX, lastHazY);
            bearHaz.physicsBody = [SKPhysicsBody bodyWithTexture:bearHaz.texture size:CGSizeMake(bearHaz.texture.size.width, bearHaz.texture.size.height)];
            bearHaz.physicsBody.dynamic = NO;
            bearHaz.physicsBody.categoryBitMask = bearCategory;
            bearHaz.physicsBody.contactTestBitMask = charCategory;
            [self addChild:bearHaz];
            lastHazX += bearHaz.frame.size.width + 400;
        }
        else if(hazType == 2){
            waterHaz = [SKSpriteNode spriteNodeWithTexture:[SKTexture textureWithImageNamed:[NSString stringWithFormat:@"more-puddle"]]];
            waterHaz.position = CGPointMake(lastHazX, lastHazY-25);
            waterHaz.physicsBody = [SKPhysicsBody bodyWithTexture:waterHaz.texture size:CGSizeMake(waterHaz.texture.size.width, waterHaz.texture.size.height)];
            waterHaz.physicsBody.dynamic = NO;
            waterHaz.physicsBody.categoryBitMask = waterCategory;
            waterHaz.physicsBody.contactTestBitMask = charCategory;
            [self addChild:waterHaz];
            lastHazX += waterHaz.frame.size.width + 400;
        }
    }

    
    SKNode *bg3 = [self childNodeWithName:@"background3"];
    lastHazX = bg3.frame.origin.x+400;
    for(int i = 0; i < 3; ++i){
        
        int hazType = arc4random_uniform(4);
        
        if(hazType == 0){
            spikeHaz = [SKSpriteNode spriteNodeWithTexture:[SKTexture textureWithImageNamed:[NSString stringWithFormat:@"gross-spikes"]]];
            spikeHaz.position = CGPointMake(lastHazX, lastHazY+50);
            spikeHaz.physicsBody = [SKPhysicsBody bodyWithTexture:spikeHaz.texture size:CGSizeMake(spikeHaz.texture.size.width, spikeHaz.texture.size.height)];
            spikeHaz.physicsBody.dynamic = NO;
            spikeHaz.physicsBody.categoryBitMask = spikeCategory;
            spikeHaz.physicsBody.contactTestBitMask = charCategory;
            [self addChild:spikeHaz];
            lastHazX += spikeHaz.frame.size.width + 400;
        }
        else if(hazType == 1){
            bearHaz = [SKSpriteNode spriteNodeWithTexture:[SKTexture textureWithImageNamed:[NSString stringWithFormat:@"Icee"]]];
            bearHaz.position = CGPointMake(lastHazX, lastHazY);
            bearHaz.physicsBody = [SKPhysicsBody bodyWithTexture:bearHaz.texture size:CGSizeMake(bearHaz.texture.size.width, bearHaz.texture.size.height)];
            bearHaz.physicsBody.dynamic = NO;
            bearHaz.physicsBody.categoryBitMask = bearCategory;
            bearHaz.physicsBody.contactTestBitMask = charCategory;
            [self addChild:bearHaz];
            lastHazX += bearHaz.frame.size.width + 400;
        }
        else if(hazType == 2){
            waterHaz = [SKSpriteNode spriteNodeWithTexture:[SKTexture textureWithImageNamed:[NSString stringWithFormat:@"more-puddle"]]];
            waterHaz.position = CGPointMake(lastHazX, lastHazY-25);
            waterHaz.physicsBody = [SKPhysicsBody bodyWithTexture:waterHaz.texture size:CGSizeMake(waterHaz.texture.size.width, waterHaz.texture.size.height)];
            waterHaz.physicsBody.dynamic = NO;
            waterHaz.physicsBody.categoryBitMask = waterCategory;
            waterHaz.physicsBody.contactTestBitMask = charCategory;
            [self addChild:waterHaz];
            lastHazX += waterHaz.frame.size.width + 400;
        }
    }

    
    SKNode *bg4 = [self childNodeWithName:@"background4"];
    lastHazX = bg4.frame.origin.x+400;
    for(int i = 0; i < 3; ++i){
        
        int hazType = arc4random_uniform(4);
        
        if(hazType == 0){
            spikeHaz = [SKSpriteNode spriteNodeWithTexture:[SKTexture textureWithImageNamed:[NSString stringWithFormat:@"gross-spikes"]]];
            spikeHaz.position = CGPointMake(lastHazX, lastHazY+50);
            spikeHaz.physicsBody = [SKPhysicsBody bodyWithTexture:spikeHaz.texture size:CGSizeMake(spikeHaz.texture.size.width, spikeHaz.texture.size.height)];
            spikeHaz.physicsBody.dynamic = NO;
            spikeHaz.physicsBody.categoryBitMask = spikeCategory;
            spikeHaz.physicsBody.contactTestBitMask = charCategory;
            [self addChild:spikeHaz];
            lastHazX += spikeHaz.frame.size.width + 400;
        }
        else if(hazType == 1){
            bearHaz = [SKSpriteNode spriteNodeWithTexture:[SKTexture textureWithImageNamed:[NSString stringWithFormat:@"Icee"]]];
            bearHaz.position = CGPointMake(lastHazX, lastHazY);
            bearHaz.physicsBody = [SKPhysicsBody bodyWithTexture:bearHaz.texture size:CGSizeMake(bearHaz.texture.size.width, bearHaz.texture.size.height)];
            bearHaz.physicsBody.dynamic = NO;
            bearHaz.physicsBody.categoryBitMask = bearCategory;
            bearHaz.physicsBody.contactTestBitMask = charCategory;
            [self addChild:bearHaz];
            lastHazX += bearHaz.frame.size.width + 400;
        }
        else if(hazType == 2){
            waterHaz = [SKSpriteNode spriteNodeWithTexture:[SKTexture textureWithImageNamed:[NSString stringWithFormat:@"more-puddle"]]];
            waterHaz.position = CGPointMake(lastHazX, lastHazY-25);
            waterHaz.physicsBody = [SKPhysicsBody bodyWithTexture:waterHaz.texture size:CGSizeMake(waterHaz.texture.size.width, waterHaz.texture.size.height)];
            waterHaz.physicsBody.dynamic = NO;
            waterHaz.physicsBody.categoryBitMask = waterCategory;
            waterHaz.physicsBody.contactTestBitMask = charCategory;
            [self addChild:waterHaz];
            lastHazX += waterHaz.frame.size.width + 400;
        }
    }
    
    score = 0;
    scoreLabel = [SKLabelNode labelNodeWithFontNamed:@"MarkerFelt-Wide"];
    scoreLabel.position = CGPointMake(character.frame.origin.x, character.frame.origin.y+700);
    [scoreLabel setZPosition:100];
    scoreLabel.text = [NSString stringWithFormat:@"%d", score];
    [self addChild:scoreLabel];
    
    
    
    gameOverLabel = [SKLabelNode labelNodeWithFontNamed:@"MarkerFelt-Wide"];
    gameOverLabel.position = CGPointMake(background.frame.origin.x/2, background.frame.origin.y/2+500);
    [gameOverLabel setZPosition:100];
    gameOverLabel.text = [NSString stringWithFormat:@"Game Over! Restarting..."];
    [gameOverLabel setHidden:true];
    [self addChild:gameOverLabel];
    
    /*
    // Get label node from scene and store it for use later
    _label = (SKLabelNode *)[self childNodeWithName:@"//helloLabel"];
    
    _label.alpha = 0.0;
    [_label runAction:[SKAction fadeInWithDuration:2.0]];
    
    CGFloat w = (self.size.width + self.size.height) * 0.05;
    
    // Create shape node to use during mouse interaction
    _spinnyNode = [SKShapeNode shapeNodeWithRectOfSize:CGSizeMake(w, w) cornerRadius:w * 0.3];
    _spinnyNode.lineWidth = 2.5;
    
    [_spinnyNode runAction:[SKAction repeatActionForever:[SKAction rotateByAngle:M_PI duration:1]]];
    [_spinnyNode runAction:[SKAction sequence:@[
                                                [SKAction waitForDuration:0.5],
                                                [SKAction fadeOutWithDuration:0.5],
                                                [SKAction removeFromParent],
                                                ]]];
     */
}

/*
- (void)touchDownAtPoint:(CGPoint)pos {
    SKShapeNode *n = [_spinnyNode copy];
    n.position = pos;
    n.strokeColor = [SKColor greenColor];
    [self addChild:n];
}

- (void)touchMovedToPoint:(CGPoint)pos {
    SKShapeNode *n = [_spinnyNode copy];
    n.position = pos;
    n.strokeColor = [SKColor blueColor];
    [self addChild:n];
}

- (void)touchUpAtPoint:(CGPoint)pos {
    SKShapeNode *n = [_spinnyNode copy];
    n.position = pos;
    n.strokeColor = [SKColor redColor];
    [self addChild:n];
}
*/

int leftTouches;
int rightTouches;
int doubleTap;
bool canJump = true;
const int moveSpeed = 400;
//Ensures animation continues while a finger is touching the screen
static const NSTimeInterval moveTime = 9999.0;

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    
    [super touchesBegan:touches withEvent:event];
    
    SKNode *character = [self childNodeWithName:@"eggy"];
    
    leftTouches = 0;
    rightTouches = 0;
    doubleTap = 0;
    for(UITouch *touch in touches){
        //Check for double tap
        if(touch.tapCount >= 2){
            doubleTap++;
        }
        //Check for left movement
        if([touch locationInNode:character.parent].x < character.position.x){
            leftTouches++;
        }
        else{//Check for right movement
            rightTouches++;
        }
    }
    
    //printf("%f\n", character.position.y);
    //Handle moving based on touches
    if(doubleTap == 1){
        //Jump behavior
        if(character.position.y <= -240.0){ //dont allow multiple jumps
            SKAction *jump;
            if(leftTouches == 1 && rightTouches == 0){
                jump = [SKAction applyImpulse:CGVectorMake(-300, 575) duration:0.2];
            }
            else{
                jump = [SKAction applyImpulse:CGVectorMake(300, 575) duration:0.2];
            }
            [character runAction:jump withKey:@"JumpAction"];
            [character runAction:self.jumpAnimation withKey:@"JumpAnimation"];
        }
    }
    
    if(leftTouches == 1 && rightTouches == 0){
        //Move left behavior
        //[character removeActionForKey:@"MoveRightAnimation"];
        character.xScale = -1.0*ABS(character.xScale);
        SKAction *moveLeft = [SKAction moveBy:CGVectorMake(-1.0 * moveSpeed * moveTime, 0) duration: moveTime];
        [character runAction: moveLeft withKey:@"MoveAction"];
        [character runAction:self.walkAnimation withKey:@"MoveAnimation"];
    }
    else if(leftTouches == 0 && rightTouches == 1){
        //Move right behavior
        //[character removeActionForKey:@"MoveLeftAnimation"];
        character.xScale = 1.0*ABS(character.xScale);
        SKAction *moveRight = [SKAction moveBy:CGVectorMake(1.0 * moveSpeed * moveTime, 0) duration: moveTime];
        [character runAction: moveRight withKey:@"MoveAction"];
        [character runAction:self.walkAnimation withKey:@"MoveAnimation"];
    }
    
    /*
    // Run 'Pulse' action from 'Actions.sks'
    [_label runAction:[SKAction actionNamed:@"Pulse"] withKey:@"fadeInOut"];
    
    for (UITouch *t in touches) {[self touchDownAtPoint:[t locationInNode:self]];}
     */
}

-(void) reduceTouches:(NSSet *)touches withEvent:(UIEvent *) event{
    SKNode *character = [self childNodeWithName:@"eggy"];
    
    for(UITouch *touch in touches){
        //Check for left touch
        if([touch locationInNode:character.parent].x < character.position.x){
            leftTouches--;
        }
        else{
            rightTouches--;
        }
    }
    
    while((leftTouches < 0 || rightTouches < 0)){
        if(leftTouches < 0){
            rightTouches += leftTouches;
            leftTouches = 0;
            
        }
        
        if(rightTouches < 0){
            leftTouches += rightTouches;
            rightTouches = 0;
        }
    }
    
    if((leftTouches + rightTouches) <= 0){
        [character removeActionForKey:@"MoveAction"];
        [character removeActionForKey:@"MoveAnimation"];
    }
}

/*
- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event{
    for (UITouch *t in touches) {[self touchMovedToPoint:[t locationInNode:self]];}
}
 */
 
- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
    //for (UITouch *t in touches) {[self touchUpAtPoint:[t locationInNode:self]];}
    [super touchesEnded:touches withEvent:event];
    [self reduceTouches:touches withEvent:event];
}

- (void)touchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event {
    //for (UITouch *t in touches) {[self touchUpAtPoint:[t locationInNode:self]];}
    [super touchesCancelled:touches withEvent:event];
    [self reduceTouches:touches withEvent:event];
}

// Called before each frame is rendered
-(void)update:(CFTimeInterval)currentTime {
    
    if(resetGame){
        SKNode *character = [self childNodeWithName:@"eggy"];
        SKNode *bg1 = [self childNodeWithName:@"background1"];
        //character.physicsBody.collisionBitMask = spikeCategory | bearCategory | waterCategory;
        [character setPosition:CGPointMake(bg1.frame.origin.x, bg1.frame.origin.y+130)];
        //character.physicsBody.velocity = CGVectorMake(0, 0);
        
        //Display game over text
        [gameOverLabel setHidden:false];
        [gameOverLabel runAction:[SKAction fadeAlphaTo:0.0 duration:2.0]];
        
        resetGame = false;
        scoreLabel.text = [NSString stringWithFormat:@"%d", score];
    }
    
    SKNode *character = [self childNodeWithName:@"eggy"];
    SKNode *bg1 = [self childNodeWithName:@"background1"];
    
    scoreLabel.position = CGPointMake(character.frame.origin.x, bg1.frame.origin.y+700);
    printf("char frame: %f\n",character.frame.origin.x);
    printf("lastPos: %f\n", lastPos);
    if(character.frame.origin.x > lastPos){
        score += 10;
        scoreLabel.text = [NSString stringWithFormat:@"%d", score];
    }
    lastPos = character.frame.origin.x;
    
    //printf("char pos: %f\n", character.position.x;
    //printf("bg x origin: %f", bg1.frame.origin.x);
    //printf("bg width*4: %f\n", bg1.frame.size.width*4-775);

    //Reset character's position when it hits the end of the frame
    //Note: 45 is just an arbitrary constant to try and smooth out the rapid repositioning of the sprite
    int distToEnd = ABS(character.frame.origin.x - ((bg1.frame.size.width*4)-775));
    if(distToEnd < 100){
        //printf("here\n");
        score+=1000;
        [character setPosition:CGPointMake(bg1.frame.origin.x-distToEnd-45, bg1.frame.origin.y+130)];
    }
    
}

-(void) didBeginContact:(SKPhysicsContact *)contact{
    //SKNode *character = [self childNodeWithName:@"eggy"];
    
    SKPhysicsBody *firstBody;
    SKPhysicsBody *secondBody;
    
    if(contact.bodyA.categoryBitMask == spikeCategory){
        firstBody = contact.bodyA;
        secondBody = contact.bodyB;
    }
    else{
        firstBody = contact.bodyB;
        secondBody = contact.bodyA;
    }
    
    if((firstBody.categoryBitMask & spikeCategory) != 0
       && (secondBody.categoryBitMask & charCategory) != 0){
        //printf("here1\n");
        [self resetScene];
    }
    else if((firstBody.categoryBitMask & bearCategory) != 0
        && (secondBody.categoryBitMask & charCategory) != 0){
        //printf("here2\n");
        [self resetScene];
    }
    else if((firstBody.categoryBitMask & waterCategory) != 0
       && (secondBody.categoryBitMask & charCategory) != 0){
        //printf("here3\n");
        [self resetScene];
    }
}

-(void) resetScene{
    //Reset sprite position
    resetGame = true;
    [gameOverLabel setAlpha:1];
    score = 0;
}

@end
